import React from 'react'
import { w3cwebsocket } from 'websocket'




/* Initial Socket Context State */
export const WSInitState: WSState = {
  socket: undefined,
  online: false,
  autoReconnect: false,
}


export enum WSActionStrings {
  EXAMPLE = 'EXAMPLE',
  SETSOCKET = 'SETSOCKET',
  SETONLINE = 'SETONLINE',
  SETAUTORECONNECT = 'SETAUTORECONNECT',
}


export type WSState = {
  socket?: w3cwebsocket
  online: boolean
  autoReconnect: boolean
}


export interface MessageObject {
  question: string,
  options: string[],
}

export type WSActions =
  | { type: WSActionStrings.EXAMPLE, payload: string }
  | { type: WSActionStrings.SETSOCKET, payload: w3cwebsocket | undefined }
  | { type: WSActionStrings.SETONLINE, payload: boolean }
  | { type: WSActionStrings.SETAUTORECONNECT, payload: boolean }


/* build the reducer */
export const WSReducer = (state: WSState, action: WSActions): WSState => {
  switch (action.type) {


    case WSActionStrings.EXAMPLE:
      return state


    case WSActionStrings.SETSOCKET:
      return { ...state, socket: action.payload }

    case WSActionStrings.SETONLINE:
      return { ...state, online: action.payload }

    case WSActionStrings.SETAUTORECONNECT:
      return { ...state, autoReconnect: action.payload }

    default:
      console.warn("DEFAULT SWITCH FALLTHROUG")
      return state
  }
}




/* Export WebSocket State Changing functions */
export const WSDispatchContext = React.createContext<React.Dispatch<WSActions>>(() => { })
/* Export WebSocket State Context */
export const WSStateContext = React.createContext<WSState>(WSInitState)