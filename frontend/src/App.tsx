import Form from "./main/Form";
import "./css/style.css";
import ConnectionState from "./appconfig/ConnectionState";
import DTModifier from "./appconfig/DTModifier";

import { useTheme } from "./appconfig/ThemeContext";
import { useEffect, useReducer, useState } from "react";
import { w3cwebsocket } from "websocket";
import WSTesting from "./main/WSTesting";
import { WSActionStrings, WSDispatchContext, WSInitState, WSReducer, WSStateContext } from "./appconfig/socket/WSContextConfig";


export interface AnswerMessage {
  selectedOption: string,
  buttonPressed: string
}


function App() {
  const [wsState, wsDispatch] = useReducer(WSReducer, WSInitState)
  const [answerObject, setAnswerObject] = useState<AnswerMessage>({ selectedOption: '', buttonPressed: '' })



  /* Effect loop to initialize socket connection. */
  useEffect(() => {
    const initializeSocket = () => {


      const newSocket = new w3cwebsocket('ws://192.168.178.13:4000')

      newSocket.onopen = () => {
        wsDispatch({ type: WSActionStrings.SETONLINE, payload: true })
      }

      newSocket.onclose = () => {
        wsDispatch({ type: WSActionStrings.SETONLINE, payload: false })
        setTimeout(() => {
          wsDispatch({ type: WSActionStrings.SETSOCKET, payload: undefined })
        }, 5000);
      }

      newSocket.onmessage = (message) => {
        if (typeof message.data === "string") {
          const answerObject: AnswerMessage = JSON.parse(message.data)
          setAnswerObject(answerObject)
        }
        // console.log(message)
      }

      wsDispatch({ type: WSActionStrings.SETSOCKET, payload: newSocket })
    }
    /* 
      Wenn noch keine socket verbindung besteht und autoReconnect aktiviert ist, baue eine auf.
    */
    if (!wsState.socket && wsState.autoReconnect) {
      initializeSocket()
    }


  }, [wsState.socket, wsState.autoReconnect])







  const darkTheme = useTheme();

  if (darkTheme === true) {
    var dt = "wrapper-d";
  } else {
    dt = "wrapper";
  }

  return (
    <WSDispatchContext.Provider value={wsDispatch}>
      <WSStateContext.Provider value={wsState}>


        <div className={dt}>
          <div className="topbar">
            <ConnectionState />
            <DTModifier />
          </div>
          <div className="container">
            <div>
              <h1 className="noselect">MESSAGEBERRY</h1>
            </div>
            <div className="form">
              <Form answerObject={answerObject} />
              {/* <WSTesting /> */}
            </div>
          </div>
        </div>
      </WSStateContext.Provider>
    </WSDispatchContext.Provider>
  );
}

export default App;
