import React, { useState } from 'react'


interface IProps {
  id: number,
  text: string
  addOptionComponent: () => void,
  removeOptionComponent: () => void
  updateOptionTextById: (id: number, newText: string) => void
  lastItem: boolean
}


const OptionComponent = ({ id, text, addOptionComponent, removeOptionComponent, updateOptionTextById, lastItem }: IProps) => {




  return (
    <div>
      {
        lastItem && id !== 1 &&

        <button
          className="minus noselect"
          onClick={removeOptionComponent}
        >
          -
            </button>
      }





      <input
        type="text"
        className="Question noselect"
        placeholder={"Option " + id}
        value={text}
        onChange={(e) => updateOptionTextById(id, e.target.value)}
      />



      {lastItem && text !== '' &&
        <button className="plus noselect" onClick={addOptionComponent}>
          +
      </button>
      }

    </div>
  )
}

export default OptionComponent