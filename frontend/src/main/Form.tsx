import React, { useContext, useState } from "react";
import { AnswerMessage } from "../App";
import OptionComponent from "./OptionComponent";
import { WSStateContext } from '../appconfig/socket/WSContextConfig'

interface IProps {
  answerObject: AnswerMessage
}

interface OptionField {
  id: number,
  text: string,
}


function Form({ answerObject }: IProps) {
  const wsState = useContext(WSStateContext)
  const [questionField, setQuestionField] = useState("");
  const [optionsArray, setOptionsArray] = useState<OptionField[]>([{ id: 1, text: '' }])



  const handleSubmit = (e: any) => {
    e.preventDefault();
    wsState.socket?.send(JSON.stringify({
      options: optionsArray.map(option => option.text),
      question: questionField
    }))

  };

  const clearAll = (e: any) => {
    e.preventDefault();
    setQuestionField("");
  };




  const addOptionComponent = () => {
    console.log('adding a option component')

    const oldOptionsArray = optionsArray
    oldOptionsArray.push({ id: optionsArray.length + 1, text: `` })
    setOptionsArray([...oldOptionsArray])
  }



  const removeOptionComponent = () => {
    console.log('removing a option component')

    const lastArray = optionsArray
    lastArray.pop()
    setOptionsArray([...lastArray]);
  }


  const updateOptionTextById = (id: number, newText: string) => {
    const mutableOptionArray = optionsArray

    mutableOptionArray[id - 1].text = newText
    setOptionsArray([...mutableOptionArray])
  }


  return (
    <form onSubmit={handleSubmit}>
      <div>
        <input
          className="Question noselect"
          type="text"
          placeholder="Frage"
          name="question"
          value={questionField}
          onChange={(e) => setQuestionField(e.target.value)}
        />
      </div>
      {optionsArray.map((option: OptionField, index: number) => {
        return (
          <OptionComponent
            key={option.id}
            id={option.id}
            text={option.text}
            addOptionComponent={addOptionComponent}
            removeOptionComponent={removeOptionComponent}
            updateOptionTextById={updateOptionTextById}
            // IF this item is the last one then give true , else give false
            lastItem={index + 1 === optionsArray.length}
          />
        )

      })}

      <div>
        <input
          className="answer noselect"
          type="text"
          placeholder="Antwort"
          value={answerObject.selectedOption}
          readOnly
        />
      </div>

      <button type="submit" className="btn noselect">
        Send
      </button>

      <button type="reset" className="btn noselect" onClick={clearAll}>
        Clear
      </button>
    </form>
  );
}

export default Form;
