import { useContext } from 'react'
import { MessageObject, WSStateContext } from '../appconfig/socket/WSContextConfig'




export default function WSTesting() {
  const wsState = useContext(WSStateContext)
  return (
    <>
      <button onClick={() => {
        const debugmessage: MessageObject = {
          options: ['Option1', 'Another', 'The Third'],
          question: 'Debug'
        }
        wsState.socket?.send(JSON.stringify(debugmessage))
      }}>Send</button>
    </>
  )
};
