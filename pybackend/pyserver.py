import asyncio
import time
import datetime
import random
import websockets
import json


import RPi.GPIO as GPIO

from luma.core.interface.serial import spi, noop
from luma.led_matrix.device import max7219
from luma.core.render import canvas
from luma.core.virtual import viewport
from luma.core.legacy import text, show_message, textsize
from luma.core.legacy.font import proportional, CP437_FONT, TINY_FONT, LCD_FONT

import multiprocessing


""" GLOBALS """
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(11, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(13, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(40, GPIO.IN, pull_up_down=GPIO.PUD_UP)


options_serial_port = spi(port=0, device=1, gpio=noop())
options_leds = max7219(options_serial_port, cascaded=3, block_orientation=-90)

question_serial_port = spi(port=0, device=0, gpio=noop())
questions_leds = max7219(question_serial_port,
                         cascaded=8, block_orientation=-90)


main_loop_running = False
option_thread = None
question_thread = None


option_counter = 0
option_statechange = True
main_loop_ready_to_run = True
option_array = []
question_string = ''

answer_button_pressed = ''
active_option = ''
send_answer = False


def select_next_option(b):
    global option_counter
    global option_statechange
    global option_array
    print('pressed next option')
    print('option array length is')
    print(len(option_array))
    print(option_counter)
    if len(option_array) > 1:
        if option_counter + 1 < len(option_array):
            option_counter += 1
        else:
            option_counter = 0
        option_statechange = True


def answer_buttons(channel):
    global main_loop_ready_to_run
    global answer_button_pressed
    print(channel)
    if channel == 13:
        answer_button_pressed = "Nein"
    elif channel == 11:
        answer_button_pressed = "Ja"
    main_loop_ready_to_run = False


def get_option_length(option):
    if option > 24:
        return option
    else:
        return 24


def get_question_length(question):
    if question > 64:
        return question
    else:
        return 64


def print_option_to_leds(option_string):
    print('Printing Option:' + option_string)
    while True:
        option_string_length = get_option_length(
            textsize(option_string, font=proportional(LCD_FONT))[0])

        virtual_option = viewport(
            options_leds, width=option_string_length, height=100)

        with canvas(virtual_option) as draw:
            text(draw, xy=(0, 0), txt=option_string,
                 fill="white", font=proportional(LCD_FONT))

        time.sleep(1)

        for offset in range(option_string_length - 24):
            virtual_option.set_position((offset, 0))
            time.sleep(0.02)

        time.sleep(1)

        for offset in range(option_string_length - 24):
            virtual_option.set_position(
                (option_string_length - 24 - 1 - offset, 0))
            time.sleep(0.02)

        time.sleep(1)


def print_question_to_leds(question):
    print('Printing Question:' + question)
    while True:
        question_string_length = get_question_length(
            textsize(question, font=proportional(LCD_FONT))[0])

        virtual_question = viewport(
            questions_leds, width=question_string_length, height=100)

        with canvas(virtual_question) as draw:
            text(draw, xy=(0, 0), txt=question,
                 fill="white", font=proportional(LCD_FONT))

        time.sleep(1)

        for offset in range(question_string_length - 64):
            virtual_question.set_position((offset, 0))
            time.sleep(0.02)

        time.sleep(1)

        for offset in range(question_string_length - 64):
            virtual_question.set_position(
                (question_string_length - 64 - 1 - offset, 0))
            time.sleep(0.02)

        time.sleep(1)


async def main_loop():
    global option_counter
    global option_statechange
    global option_thread
    global option_array
    global options_leds
    global active_option
    global question_thread
    global question_string

    while main_loop_ready_to_run:
        if question_thread == None:
            print('creating question thread')
            question_thread = multiprocessing.Process(
                target=print_question_to_leds, args=(question_string,))
            question_thread.start()

        if option_statechange:
            if option_thread != None and option_thread.is_alive():
                print('killing option thread')
                option_thread.terminate()

            # print_option_task = asyncio.ensure_future(print_option_to_leds())
            # print('Starting async print options')
            # done, pending = await asyncio.wait(
            #     [print_option_task],
            #     return_when=asyncio.FIRST_COMPLETED
            # )

            active_option = option_array[option_counter]
            option_thread = multiprocessing.Process(
                target=print_option_to_leds, args=(active_option,))
            print('starting thread')
            option_thread.start()
            option_statechange = False

        time.sleep(0.2)

    print('end main loop running')
    if option_thread != None and option_thread.is_alive():
        print('killing final option thread')
        option_thread.terminate()
        option_thread = None
        question_thread.terminate()
        question_thread = None
        options_leds.clear()
        questions_leds.clear()


async def consumer(jsonmessage):
    global main_loop_running
    global option_array
    global main_loop_ready_to_run
    global option_statechange
    global option
    global send_answer
    global option_counter
    global question_string

    msg = json.loads(jsonmessage)

    question_string = msg["question"]
    option_array = msg["options"]
    if main_loop_running is False:
        main_loop_running = True
        await main_loop()
        main_loop_running = False
        main_loop_ready_to_run = True
        option_statechange = True
        send_answer = True
        option_array = []
        option_counter = 0
        question_string = ''


def build_answer_string():
    global active_option
    global answer_button_pressed
    return json.dumps({
        "selectedOption": active_option,
        "buttonPressed": answer_button_pressed
    }, indent=4)


async def pong():
    global answer_button_pressed
    global send_answer
    while not send_answer:
        await asyncio.sleep(0.2)

    send_answer = False
    return build_answer_string()


async def handle_websocket(websocket, path):
    while True:
        listener_task = asyncio.ensure_future(websocket.recv())
        producer_task = asyncio.ensure_future(pong())
        print('Waiting for Messages...')
        done, pending = await asyncio.wait(
            [listener_task, producer_task],
            return_when=asyncio.FIRST_COMPLETED
        )

        if listener_task in done:
            print('listen done')
            message = listener_task.result()
            await consumer(message)
        else:
            print('listen cancel')
            listener_task.cancel()

        if producer_task in done:
            print('producer done')
            message = producer_task.result()
            await websocket.send(message)
        else:
            print('producer cancel')
            producer_task.cancel()


# async def handle_websocket(websocket, path):
#     while True:
#         listener_task = asyncio.ensure_future(websocket.recv())
#         print('Waiting for Messages...')
#         done, pending = await asyncio.wait(
#             [listener_task],
#             return_when=asyncio.FIRST_COMPLETED
#         )

#         if listener_task in done:
#             print('listen done')
#             message = listener_task.result()
#             await consumer(message)
#         else:
#             print('listen cancel')
#             listener_task.cancel()


GPIO.add_event_detect(
    11, GPIO.FALLING, callback=answer_buttons, bouncetime=500)
GPIO.add_event_detect(
    13, GPIO.FALLING, callback=answer_buttons, bouncetime=500)
GPIO.add_event_detect(
    40, GPIO.FALLING, callback=select_next_option, bouncetime=500)


start_server = websockets.serve(handle_websocket, "192.168.178.13", 4000)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
