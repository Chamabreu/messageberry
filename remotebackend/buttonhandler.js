var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
var yesButton = new Gpio(17, 'in', 'rising'); //use GPIO pin 17 as input, and 'both' button presses, and releases should be handled
var noButton = new Gpio(27, 'in', 'rising'); //use GPIO pin 17 as input, and 'both' button presses, and releases should be handled




module.exports.getAnswer = () => {
  return new Promise((resolve, reject) => {

    yesButton.watch(function (err, value) { //Watch for hardware interrupts on pushButton GPIO, specify callback function
      if (err) { //if an error
        console.error('There was an error', err); //output error message to console
        return;
      } else {
        console.log('pressed Yes')
        resolve(true)
        yesButton.unwatchAll()
        noButton.unwatchAll()
      }
    });


    noButton.watch(function (err, value) { //Watch for hardware interrupts on pushButton GPIO, specify callback function
      if (err) { //if an error
        console.error('There was an error', err); //output error message to console
        return;
      } else {
        console.log('pressed No')
        resolve(false)
        noButton.unwatchAll()
        yesButton.unwatchAll()
      }
    });

  })
}
