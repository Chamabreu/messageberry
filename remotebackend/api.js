var Gpio = require('onoff').Gpio; //include onoff to interact with the GPIO
var yesButton = new Gpio(17, 'in', 'rising'); //use GPIO pin 17 as input, and 'both' button presses, and releases should be handled
var noButton = new Gpio(27, 'in', 'rising'); //use GPIO pin 17 as input, and 'both' button presses, and releases should be handled

const router = require('express').Router()

const { spawn } = require('child_process')


function getAnswer(req, res, next) {

  yesButton.watch(function (err, value) { //Watch for hardware interrupts on pushButton GPIO, specify callback function
    if (err) { //if an error
      console.error('There was an error', err); //output error message to console
      return;
    } else {
      res.ans = 'yes'
      res.script.kill('SIGINT')
      next()
    }
  });


  noButton.watch(function (err, value) { //Watch for hardware interrupts on pushButton GPIO, specify callback function
    if (err) { //if an error
      console.error('There was an error', err); //output error message to console
      return;
    } else {
      res.ans = 'no'
      res.script.kill('SIGINT')
      next()
    }
  });

}


function cleanButtons(req, res, next) {
  yesButton.unwatchAll()
  noButton.unwatchAll()
  next()
}


function askMsg(req, res, next) {
  const msg = req.body.msg

  console.log('Asking:', msg)
  const pythonscript = spawn('python3', [`./pythonscripts/msg.py`, `--text`, `${msg}`])
  pythonscript.on("exit", () => { console.log('exited') })
  res.script = pythonscript
  next()
}


function sendAnswer(req, res) {
  console.log('answer is', res.ans)
  res.send(res.ans)

}

router.post('/msg', askMsg, getAnswer, cleanButtons, sendAnswer)



module.exports = router