const { spawn } = require('child_process')
const { getAnswer } = require('../buttonhandler')
module.exports.ask = (message) => {
  console.log('asking message', message)

  const pythonscript = spawn('python3', [`./pythonscripts/msg.py`, `--text`, `${message}`])
  pythonscript.on("exit", () => { console.log('script exited') })

  return new Promise((resolve, reject) => {

    getAnswer().then(response => {
      pythonscript.kill('SIGINT')
      resolve(response)
    })
  })
}