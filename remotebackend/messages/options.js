
const { spawn } = require('child_process')
const { getAnswer } = require('../buttonhandler')



module.exports.options = (optionsArray) => {
  console.log('setting options', optionsArray)

  const pythonscript = spawn('python3', [`./pythonscripts/options.py`, `--option1`, `${optionsArray[0]}`, `--option2`, `${optionsArray[1]}`, `--option3`, `${optionsArray[2]}`])
  pythonscript.on("exit", () => { console.log('script exited') })

  return new Promise((resolve, reject) => {

    getAnswer().then(response => {
      pythonscript.kill('SIGINT')
      resolve(response)
    })
  })
}