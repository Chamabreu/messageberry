
const { spawn } = require('child_process')
const { getAnswer } = require('../buttonhandler')



module.exports.testscript = (message) => {
  console.log('test script with', message)

  const pythonscript = spawn('python3', [`./pythonscripts/testscript.py`, `--text`, message])
  pythonscript.on("exit", () => { console.log('script exited') })

  return new Promise((resolve, reject) => {

    getAnswer().then(response => {
      pythonscript.kill('SIGINT')
      resolve(response)
    })
  })
}