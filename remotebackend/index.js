const path = require('path')
const express = require('express')
const app = express()
const apirouter = require('./api')
const port = 3000
const cors = require('cors')

app.use(cors())



app.use(express.json())
app.use('/api', apirouter)
app.use('/', express.static(path.join(__dirname, 'build')))
app.use('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'build'))
})


app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`)
})

