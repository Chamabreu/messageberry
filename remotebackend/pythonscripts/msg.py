import time
from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
from luma.led_matrix.device import max7219
from luma.core.virtual import viewport
from luma.core.legacy import text, show_message, textsize
from luma.core.legacy.font import proportional, CP437_FONT, TINY_FONT, LCD_FONT
import argparse



questionserial = spi(port=0, device=0, gpio=noop())
answerserial = spi(port=0, device=1, gpio=noop())
question = max7219(questionserial, cascaded=8, block_orientation=-90)
answer = max7219(answerserial, cascaded=3, block_orientation=-90)

def getquestionlength(question):
    if question > 64:
        return question
    else:
        return 64


def runner(message):
    questionstring = message
    show_message(question, questionstring, fill="white", font=proportional(LCD_FONT))

def static(message):
    running = True 
    counter = 0
    endafterseconds = 1000
    questionstring = message
    answerstring = ""
    questionlength = getquestionlength(textsize(questionstring, font=proportional(LCD_FONT))[0])



    virtual_question = viewport(question, width=questionlength, height=100)
    virtual_answer = viewport(answer, width=200, height=100)

    try:
        while running:
            with canvas(virtual_question) as draw:
                text(draw, xy=(0, 0), txt=questionstring, fill="white", font=proportional(LCD_FONT))
                #draw.text((0, 0), txt="hi there", fill="white", font=proportional(LCD_FONT))
                
            print(questionlength)
            time.sleep(1)

            print(virtual_question.width)
            for offset in range(questionlength - 64):
                print(offset)
                virtual_question.set_position((offset, 0))
                time.sleep(0.02)

            time.sleep(1) 

            for offset in range(questionlength - 64):
                print('back',questionlength - 64 - 1 - offset)
                virtual_question.set_position((questionlength - 64 - 1 - offset, 0))
                time.sleep(0.02)

            #with canvas(answer) as draw:
                #text(draw, xy=(0, 0), txt=answerstring, fill="white", font=proportional(LCD_FONT))

            counter += 1
            running = True if counter <= endafterseconds else False

    except KeyboardInterrupt:
        pass



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Arguments for msg.py',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--text', type=str, default='abcdefghijklmnopqrstuvwxyz', help='The Message to print')

    args = parser.parse_args()

    try:
        static(args.text)
        #runner(args.text)
    except KeyboardInterrupt:
        pass
