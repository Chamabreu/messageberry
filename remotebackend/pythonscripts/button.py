import RPi.GPIO as GPIO

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(40, GPIO.IN, pull_up_down=GPIO.PUD_UP)
if GPIO.wait_for_edge(40, GPIO.FALLING):
    GPIO.cleanup()

