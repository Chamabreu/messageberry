import time
from luma.core.interface.serial import spi, noop
from luma.core.render import canvas
from luma.led_matrix.device import max7219
from luma.core.virtual import viewport
from luma.core.legacy import text, show_message, textsize
from luma.core.legacy.font import proportional, CP437_FONT, TINY_FONT, LCD_FONT
import argparse
import RPi.GPIO as GPIO
import multiprocessing

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(40, GPIO.IN, pull_up_down=GPIO.PUD_UP)

counter = 0
statechange = True
active_option = ''
option1 = ''
option2 = ''
option3 = ''
message_thread = None


answerserial = spi(port=0, device=1, gpio=noop())
answer = max7219(answerserial, cascaded=3, block_orientation=-90)


def getanswerlength(answer):
    if answer > 24:
        return answer
    else:
        return 24


def showmessage(message):
    while True:
        answerlength = getanswerlength(
            textsize(message, font=proportional(LCD_FONT))[0])

        virtual_answer = viewport(answer, width=answerlength, height=100)

        with canvas(virtual_answer) as draw:
            text(draw, xy=(0, 0), txt=message,
                 fill="white", font=proportional(LCD_FONT))

        time.sleep(1)

        for offset in range(answerlength - 24):
            virtual_answer.set_position((offset, 0))
            time.sleep(0.02)

        time.sleep(1)

        for offset in range(answerlength - 24):
            virtual_answer.set_position((answerlength - 24 - 1 - offset, 0))
            time.sleep(0.02)

        time.sleep(1)


def get_new_message():
    global counter
    global option1
    global option2
    global option3

    if counter == 0:
        return option1
    elif counter == 1:
        return option2
    elif counter == 2:
        return option3
    else:
        counter = 0
        return option1


def main_loop(input1, input2, input3):
    global counter
    global statechange
    global active_option
    global message_thread

    global option1
    global option2
    global option3
    option1 = input1
    option2 = input2
    option3 = input3

    active_option = get_next_option()

    def next_option(channel):
        global counter
        global statechange
        print('pressed next option')
        counter += 1
        statechange = True
    GPIO.add_event_detect(
        40, GPIO.RISING, callback=next_option, bouncetime=200)

    while True:
        if statechange:
            if message_thread != None and message_thread.is_alive():
                message_thread.terminate()

            active_option = get_next_option()
            message_thread = multiprocessing.Process(
                target=showmessage, args=(active_option,))
            message_thread.start()
            statechange = False

        time.sleep(0.2)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Arguments for msg.py',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--option1', type=str,
                        default='option1', help='option 1')
    parser.add_argument('--option2', type=str,
                        default='option2', help='option 2')
    parser.add_argument('--option3', type=str,
                        default='option3', help='option 3')

    args = parser.parse_args()

    try:
        main_loop(args.option1, args.option2, args.option3)

    except KeyboardInterrupt:
        message_thread.terminate()
        pass
