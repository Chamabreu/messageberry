import threading
import time

def threadFunc():
	for i in range(5):
		print('hello from threading')
		time.sleep(1)

print('before calling')

th = threading.Thread(target=threadFunc)
th.start()
print('after calling')

exit = input('enter to exit')
