const app = require('express')();
const cors = require("cors");
const { ask } = require('./messages/ask');
const { options } = require('./messages/options')
const { testscript } = require('./messages/testscript')
app.use(cors())


const server = require('http').createServer(app);
const io = require('socket.io')(server);

const answer = (socket, response) => {
  socket.emit('response', response)
}

io.on('connection', (socket) => {
  socket.on('ask', (message) => {
    ask(message)
      .then(response => { answer(socket, response) })
  })
  socket.on('options', (optionsArray) => {
    options(optionsArray)
  })


  socket.on('test', (message) => {
    testscript(message)
  })
})



server.listen('3000', () => { console.log('server is up') })