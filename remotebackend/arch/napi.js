
const router = require('express').Router()

const { spawn } = require('child_process')

router.post('/msg', (req, res) => {
  const msg = req.body.msg
  console.log(`Received MSG:`, msg)



  res.send('no!!!!')
  // const pythonscript = spawn('python3', [`./pythonscripts/msg.py`])
  // const pythonscript = spawn('python3', [`./pythonscripts/msg.py`, `--text`, `${msg}`])
})


router.get('/:execute', (req, res) => {
  const query = req.query



  const executeCMD = req.params.execute
  console.log(`Starting ${executeCMD} script.`)
  const pythonscript = spawn('python3', [`./pythonscripts/${executeCMD}.py`])
  pythonscript.on('close', () => {

    res.send(`Script ${executeCMD} wurde beendet.`)
  })
})

module.exports = router